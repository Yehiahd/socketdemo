package com.fci.yehiahd.socketdemo.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.fci.yehiahd.socketdemo.R;
import com.fci.yehiahd.socketdemo.util.CircleTransform;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by yehia on 20/04/17.
 */

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ChatHolder> {

    private List<String> messageList;
    private Context mContext;

    public ChatAdapter(List messageList, Context context) {
        this.messageList = messageList;
        this.mContext = context;
    }

    public void updateChatList(List<String> newList) {
        messageList.clear();
        messageList.addAll(newList);
        this.notifyDataSetChanged();
    }


    @Override
    public ChatAdapter.ChatHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_row, parent, false);

        return new ChatHolder(view);
    }

    @Override
    public void onBindViewHolder(ChatAdapter.ChatHolder holder, int position) {
        holder.userMsg.setText(messageList.get(position));
        holder.userName.setText("Yehia");

        Picasso.with(mContext)
                .load(R.drawable.profile)
                .transform(new CircleTransform())
                .into(holder.userImg);
    }

    @Override
    public int getItemCount() {
        return messageList.size();
    }

    public class ChatHolder extends RecyclerView.ViewHolder {

        private ImageView userImg;
        private TextView userMsg;
        private TextView userName;


        public ChatHolder(View itemView) {
            super(itemView);

            userImg = (ImageView) itemView.findViewById(R.id.user_chat_img);
            userMsg = (TextView) itemView.findViewById(R.id.user_chat_msg);
            userName = (TextView) itemView.findViewById(R.id.user_name);
        }
    }
}
