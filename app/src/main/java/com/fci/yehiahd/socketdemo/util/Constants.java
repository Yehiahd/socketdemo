package com.fci.yehiahd.socketdemo.util;

/**
 * Created by yehia on 20/04/17.
 */

public interface Constants {

    interface Urls {
        String LOCAL_HOST = "http://192.168.1.35:3005";
    }

    interface Nodes {
        String ADD_USER = "adduser";
        String START_CHAT = "startchat";
        String SEND_MESSAGE = "sendchat";
        String GET_MESSAGE = "updatechat";
        String GET_HISTORY = "history";
    }
}
