package com.fci.yehiahd.socketdemo.controller.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.fci.yehiahd.socketdemo.Helper.SocketHelper;
import com.fci.yehiahd.socketdemo.R;
import com.fci.yehiahd.socketdemo.adapter.ChatAdapter;
import com.fci.yehiahd.socketdemo.util.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.socket.client.Socket;
import io.socket.emitter.Emitter;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private RecyclerView mRecyclerView;
    private EditText sendMessageET;
    private ChatAdapter mChatAdapter;
    private LinearLayoutManager mLayoutManager;
    private List<String> mList;
    private ImageView sendImg;

    Socket mSocket;
    JSONObject jsonObject;
    private static final String MY_ID = "1222";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mSocket = SocketHelper.getSocket();
        prepJson();
        initUi();
        setUpUi();
        mSocket.emit(Constants.Nodes.START_CHAT, jsonObject);
        mSocket.on(Constants.Nodes.GET_HISTORY, new Emitter.Listener() {
            @Override
            public void call(final Object... args) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Log.d("History",String.valueOf(args));
                        JSONArray jsonArray = (JSONArray) args[0];
                        if ( String.valueOf(args[1] ).equals(MY_ID)) {
                            for (int i = 0; i < jsonArray.length(); i++) {
                                try {
                                    mList.add(jsonArray.getJSONObject(i).getString("message"));
                                    mChatAdapter.notifyDataSetChanged();
                                    mRecyclerView.scrollToPosition(mList.size() - 1);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                });

            }
        });
        mSocket.on(Constants.Nodes.GET_MESSAGE, new Emitter.Listener() {
            @Override
            public void call(final Object... args) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(String.valueOf(args[1]));
                            mList.add(jsonObject.getString("message"));
                            mChatAdapter.notifyDataSetChanged();
                            mRecyclerView.scrollToPosition(mList.size() - 1);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });

            }
        });
//        RxSocket.getMessage()
//                .subscribeOn(Schedulers.newThread())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new Action1<String>() {
//                    @Override
//                    public void call(String s) {
//                        if (mChatAdapter != null){
//                            mList.add(s);
//                            mChatAdapter.updateChatList(mList);
//                        }
//                    }
//                }, new Action1<Throwable>() {
//                    @Override
//                    public void call(Throwable throwable) {
//                        Toast.makeText(MainActivity.this, throwable.getMessage(), Toast.LENGTH_SHORT).show();
//                    }
//                });
        mSocket.connect();

    }

    private void prepJson() {
        try {
            jsonObject = new JSONObject("{username:\"Yehia\",\n" +
                    "username2:\"Mahmoud\",\n" +
                    "public_id2:1234,\n" +
                    "public_id:1222,\n" +
                    "pushNotifications: true,\n" +
                    "pushNotificationToken: \"hoass\",\n" +
                    "pushNotifications2: true,\n" +
                    "pushNotificationToken2: \"hoass\",\n" +
                    "deviceType: 1,\n" +
                    "deviceType2: 1}");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void initUi() {
        mList = new ArrayList<>();
        mRecyclerView = (RecyclerView) findViewById(R.id.chat_recycler);
        sendMessageET = (EditText) findViewById(R.id.send_new_message_ET);
        sendImg = (ImageView) findViewById(R.id.send_img);
        sendImg.setOnClickListener(this);
    }

    private void setUpUi() {
        mChatAdapter = new ChatAdapter(mList, this);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mChatAdapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.send_img:
                String message = sendMessageET.getText().toString();
                if (!message.equals("")) {
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("message", message);
                        jsonObject.put("publicId", 1222);
                        mSocket.emit(Constants.Nodes.SEND_MESSAGE, jsonObject);
                        sendMessageET.setText("");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                break;
        }
    }
}
