package com.fci.yehiahd.socketdemo.api;

import com.fci.yehiahd.socketdemo.Helper.SocketHelper;
import com.fci.yehiahd.socketdemo.util.Constants;

import io.socket.emitter.Emitter;
import rx.AsyncEmitter;
import rx.Observable;
import rx.functions.Action1;

/**
 * Created by yehia on 20/04/17.
 */

public class RxSocket {

    public static Observable<String> getMessage () {
        return Observable.fromEmitter(new Action1<AsyncEmitter<String>>() {
            @Override
            public void call(final AsyncEmitter<String> emitter) {
                SocketHelper.getSocket().on(Constants.Nodes.GET_MESSAGE, new Emitter.Listener() {
                    @Override
                    public void call(Object... args) {

                        try {
                            emitter.onNext(String.valueOf(args[1]));
                            emitter.onCompleted();
                        }
                        catch (Exception e){
                            emitter.onError(e);
                        }

                    }
                });
            }
        }, AsyncEmitter.BackpressureMode.NONE);
    }

}
