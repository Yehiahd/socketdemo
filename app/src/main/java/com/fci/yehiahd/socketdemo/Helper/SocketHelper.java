package com.fci.yehiahd.socketdemo.Helper;

import com.fci.yehiahd.socketdemo.util.Constants;

import io.socket.client.IO;
import io.socket.client.Socket;

/**
 * Created by yehia on 20/04/17.
 */

public class SocketHelper {

    private static Socket mSocket;

    public static Socket getSocket() {
        if (mSocket == null) {
            try {
                mSocket = IO.socket(Constants.Urls.LOCAL_HOST);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return mSocket;
    }

}
